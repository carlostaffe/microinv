FROM yiisoftware/yii2-php:7.4-apache
WORKDIR /app
COPY . /app
#ENV APACHE_RUN_DIR=/var/run/apache2
#ENV APACHE_RUN_USER=www-data
RUN composer install
EXPOSE 80

ENTRYPOINT ["apachectl", "-D" , "FOREGROUND"]
